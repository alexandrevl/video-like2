var fs = require("fs");
var _ = require("lodash");
var readline = require("readline");
var { google } = require("googleapis");
var OAuth2 = google.auth.OAuth2;
const axios = require("axios");
const moedas = require("./moedas.json");
const moedasBr = require("./moedas_br.json");

let videoId = "H3Zt3vq57T0";

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/youtube-nodejs-quickstart.json
var SCOPES = [
  "https://www.googleapis.com/auth/yt-analytics.readonly",
  "https://www.googleapis.com/auth/youtube",
  "https://www.googleapis.com/auth/youtubepartner",
  "https://www.googleapis.com/auth/youtube.force-ssl"
];
var TOKEN_DIR =
  (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) +
  "/.credentials/";
var TOKEN_PATH = "./youtube-nodejs-quickstart.json";

// Load client secrets from a local file.
function start() {
  console.log("Start update title");
  fs.readFile("client.json", function processClientSecrets(err, content) {
    if (err) {
      console.log("Error loading client secret file: " + err);
      return;
    }
    // Authorize a client with the loaded credentials, then call the YouTube API.
    authorize(JSON.parse(content), getStatsVideo);
  });
}

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  var clientSecret = credentials.installed.client_secret;
  var clientId = credentials.installed.client_id;
  var redirectUrl = credentials.installed.redirect_uris[0];
  var oauth2Client = new OAuth2(clientId, clientSecret, redirectUrl);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, function(err, token) {
    if (err) {
      getNewToken(oauth2Client, callback);
    } else {
      oauth2Client.credentials = JSON.parse(token);
      callback(oauth2Client);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {
  var authUrl = oauth2Client.generateAuthUrl({
    access_type: "offline",
    scope: SCOPES
  });
  console.log("Authorize this app by visiting this url: ", authUrl);
  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question("Enter the code from that page here: ", function(code) {
    rl.close();
    oauth2Client.getToken(code, function(err, token) {
      if (err) {
        console.log("Error while trying to retrieve access token", err);
        return;
      }
      oauth2Client.credentials = token;
      storeToken(token);
      callback(oauth2Client);
    });
  });
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
  try {
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code != "EEXIST") {
      throw err;
    }
  }
  fs.writeFile(TOKEN_PATH, JSON.stringify(token), err => {
    if (err) throw err;
    console.log("Token stored to " + TOKEN_PATH);
  });
}

let qntLikes = 0;
/**
 * Lists the names and IDs of up to 10 files.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function getStatsVideo(auth) {
  var service = google.youtube("v3");
  // var serviceAnalytics = google.youtubeAnalytics("v2");
  // serviceAnalytics.reports.query({
  //   endDate: "2020-05-31",
  //   ids: "channel==MINE",
  //   metrics: "subscribersGained,subscribersLost",
  //   startDate: "2017-01-01"
  // }, (err, response) => {
  //   console.log(err)
  //   console.log(response);
  // });
  service.videos.list(
    {
      auth: auth,
      part: "statistics",
      id: videoId
    },
    function(err, response) {
      if (err) {
        console.log("The API returned an error: " + err);
        return;
      }
      console.log(response.data.items);
      if (qntLikes != response.data.items[0].statistics.likeCount) {
        qntLikes = response.data.items[0].statistics.likeCount;
        getLastCommentAuthorName(auth).then(authorDisplayName => {
          let title = `${qntLikes} likes com o comentário incrível de ${authorDisplayName}`;
          updateTitle(auth, title);
        });
      } else {
        console.log("Não alterado. Likes não aumentaram.");
      }
    }
  );
}

function getNameChannel(auth, channelId) {
  var service = google.youtube("v3");
  service.channels
    .list({
      auth: auth,
      part: ["snippet"],
      id: ["UCCFcBOBr7B3ob4JjyTRdTRQ"]
    })
    .then(response => {
      console.log(response.data.items);
    });
}

function getLastCommentAuthorName(auth) {
  return new Promise(resolve => {
    var service = google.youtube("v3");
    service.commentThreads
      .list({
        auth: auth,
        part: ["snippet"],
        order: "time",
        videoId: videoId
      })
      .then(response => {
        resolve(
          response.data.items[0].snippet.topLevelComment.snippet
            .authorDisplayName
        );
        // response.data.items.forEach(items => {
        //   console.log(items.snippet.topLevelComment.snippet.authorDisplayName);
        // });
      });
  });
}

function updateTitle(auth, text) {
  var service = google.youtube("v3");
  service.videos.update(
    {
      auth: auth,
      part: "snippet",
      resource: {
        id: videoId,
        snippet: {
          title: text,
          categoryId: "24",
          description: `█ NOSSO DISCORD: https://discord.gg/JYFH6HX
█ NOSSO INSTAGRAM: https://www.instagram.com/mr_guinas/
█ NOSSO TWITTER: https://twitter.com/Mr_Guinas
█ A ROXA: https://bit.ly/2yMJdeA

-------------------------------------------------

Mais uma vez, muito obrigado pelo seu gostei! Você é uma das pessoas que mais me impulsiona a continuar com o trabalho!

-------------------------------------------------`,
          defaultLanguage: "pt-BR",
          defaultAudioLanguage: "pt-BR",
          tags: [
            "mrguinas",
            "guinas",
            "memes engraçados",
            "lubatv",
            "huestation",
            "core das antigas",
            "memes discord",
            "oshi",
            "tocadocoelho",
            "o ser humano na internet",
            "mr guinas",
            "likes no titulo",
            "gamer aleatorio",
            "cinema dos memes",
            "nice guys",
            "para rir",
            "esse video tem",
            "likes",
            "moedas",
            "likes que mudam",
            "titulo que muda",
            "gamer aleatorio",
            "gamer",
            "aleatorio",
            "desenho animado",
            "inscritos",
            "views",
            "likes",
            "comentários"
          ]
        }
        // tags: "mrguinas, guinas, lubatv",
      }
    },
    function(err, response) {
      if (err) {
        console.log("The API returned an error: " + err);
        return;
      }
      console.log(response.data);
    }
  );
}

function getRandomInt(min, max) {
  //console.log(min,max);
  if (Math.ceil(min) == Math.floor(max)) {
    return min;
  } else {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
setInterval(() => {
  start();
}, 600 * 1000);
start();
