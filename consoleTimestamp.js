const moment = require("moment");

module.exports = function() {
  originalLog = console.log;
  // Overwriting
  console.log = function() {
    var args = [].slice.call(arguments);
    originalLog.apply(console.log, [getCurrentDateString()].concat(args));
  };
  // Returns current timestamp
  function getCurrentDateString() {
    //return new Date().toISOString() + " ::";
    return moment().format("HH:mm:ss.SSS") + " ::";
  }
};
