var fs = require("fs");

function main() {
  let resultado = "";
  fs.createReadStream("current.city.list.json")
    .on("data", function (chunk) {
      //console.log("Chunk:", chunk.toString());
      resultado += chunk.toString();
      //console.log(resultado.length);
    })
    .on("end", function () {
      let fileName = "./br.json";
      let writer = fs.createWriteStream(fileName);
      resultado = JSON.parse(resultado);
      let fim = [];
      resultado.forEach((item) => {
        if (item.country == "BR") {
          fim.push(item);
        }
      });
      writer.write(JSON.stringify(fim));
      writer.end();
    });
}
main();
