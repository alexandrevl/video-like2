var fs = require("fs");
const brCities = require("./br.json");

function main() {
  console.log(brCities[getRandomInt(0, brCities.length - 1)]);
}

function getRandomInt(min, max) {
  //console.log(min,max);
  if (Math.ceil(min) == Math.floor(max)) {
    return min;
  } else {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
main();
